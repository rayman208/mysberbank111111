﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsClient.Controllers;

namespace WindowsFormsClient.Views
{
    public partial class FormAuth : Form
    {
        private ControllerFormAuth controller;

        public FormAuth()
        {
            InitializeComponent();
        }

        private void FormAuth_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormAuth(this);
        }

        private void buttonAuthorize_Click(object sender, EventArgs e)
        {
            controller.AuthorizeAsync();
        }
    }
}
