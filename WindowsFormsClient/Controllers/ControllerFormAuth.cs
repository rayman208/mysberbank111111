﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsClient.Model;
using WindowsFormsClient.Views;
using EntitiesClassLibrary;

namespace WindowsFormsClient.Controllers
{
    class ControllerFormAuth
    {
        private ApiWorker api;
        private FormAuth form;

        public ControllerFormAuth(FormAuth form)
        {
            this.api = ApiWorker.GetInstance();
            this.form = form;
        }

        public async void AuthorizeAsync()
        {
            string login = form.textBoxLogin.Text;

            if (login == "")
            {
                MessageBox.Show("Ошибка. Введите логин!");
                return;
            }

            string password = form.textBoxPassword.Text;
            if (password == "")
            {
                MessageBox.Show("Ошибка. Введите пароль!");
                return;
            }


            try
            {
                User user = await api.GetUserByLoginAndPassword(login, password);
                MessageBox.Show($"Добро пожаловать {user.Name}");
            }
            catch (Exception e)
            {
                MessageBox.Show($"Ошибка. {e.Message}");
            }
        }
    }
}
