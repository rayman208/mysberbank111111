﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientClassLibrary;
using CommunicationClassLibrary;
using EntitiesClassLibrary;
using Newtonsoft.Json;

namespace WindowsFormsClient.Model
{
    class ApiWorker
    {
        private static ApiWorker instance = null;

        private ClientManager clientManager;
        private string key = "sjkJHGHJSDHJ732846kjhsdfgh8239dshf";

        private ApiWorker()
        {
            clientManager = new ClientManager();
        }

        public static ApiWorker GetInstance()
        {
            if (instance == null)
            {
                instance = new ApiWorker();
            }

            return instance;
        }

        public async Task<User> GetUserByLoginAndPassword(string login, string password)
        {
            User requestUser = new User()
            {
                Login = login,
                Password = password
            };

            Request request = new Request()
            {
                Command = "users.select.by_login_and_password",
                Parameters = JsonConvert.SerializeObject(requestUser),
                Key = key
            };

            Response response = await clientManager.MakeRequestToServer(request);

            if (response.Status == "ERROR")
            {
                throw new Exception(response.Message);
            }

            if (response.Status == "OK")
            {
                return JsonConvert.DeserializeObject<User>(response.Message);
            }

            return null;
        }
    }
}
