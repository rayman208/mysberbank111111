﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CommunicationClassLibrary;
using Newtonsoft.Json;

namespace ServerClassLibrary
{
    public class ServerManager
    {
        private HttpListener httpListener;

        public ServerManager()
        {
            httpListener = new HttpListener();
            httpListener.Prefixes.Add("http://localhost:12345/connection/");
            httpListener.Start();
        }

        public HttpListenerContext GetClientContext()
        {
            return httpListener.GetContext();
        }

        public static Request RecieveRequestFromClient(HttpListenerContext clientContext)
        {
            HttpListenerRequest requestContext = clientContext.Request;

            StreamReader requestStream = new StreamReader(requestContext.InputStream, requestContext.ContentEncoding);

            string data = requestStream.ReadToEnd();

            requestStream.Close();

            return JsonConvert.DeserializeObject<Request>(data);
        }

        public static void SendResponseToClient(HttpListenerContext clientContext, Response response)
        {
            HttpListenerResponse responseContext = clientContext.Response;

            string data = JsonConvert.SerializeObject(response);

            byte[] bytes = Encoding.UTF8.GetBytes(data);
            responseContext.ContentLength64 = bytes.LongLength;

            Stream responseStream = responseContext.OutputStream;
            responseStream.Write(bytes, 0, bytes.Length);

            responseStream.Close();
        }
    }
}
