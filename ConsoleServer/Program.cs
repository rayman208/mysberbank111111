﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CommunicationClassLibrary;
using DbManagerClassLibrary;
using EntitiesClassLibrary;
using Newtonsoft.Json;
using ServerClassLibrary;

namespace ConsoleServer
{
    class Program
    {
        static void Log(string msg)
        {
            Console.WriteLine($"{DateTime.Now}: {msg}");
        }

        static void ProccessClient(HttpListenerContext clientContext)
        {
            DbManager db = new DbManager();

            Log($"Client connected");

            Response response = null;
            Request request = null;

            try
            {
                request = ServerManager.RecieveRequestFromClient(clientContext);
                Log($"Request from client {request}");
            }
            catch (Exception e)
            {
                response = new Response()
                {
                    Status = "ERROR",
                    Message = e.Message
                };
            }


            if (request != null)
            {
                if (request.Key != "sjkJHGHJSDHJ732846kjhsdfgh8239dshf")
                {
                    response = new Response()
                    {
                        Status = "ERROR",
                        Message = "Wrong API key"
                    };
                }
                else
                {
                    try
                    {
                        switch (request.Command)
                        {
                            case "users.select.by_login_and_password":
                                {
                                    User requestUser = JsonConvert.DeserializeObject<User>(request.Parameters);

                                    User findUser = db.TableUsers.GetUserByLoginAndPassword(requestUser.Login, requestUser.Password);

                                    if (findUser == null)
                                    {
                                        response = new Response()
                                        {
                                            Status = "ERROR",
                                            Message = "Unknown user"
                                        };
                                    }
                                    else
                                    {
                                        response = new Response()
                                        {
                                            Status = "OK",
                                            Message = JsonConvert.SerializeObject(findUser)
                                        };
                                    }

                                }
                                break;

                            default:
                                response = new Response()
                                {
                                    Status = "ERROR",
                                    Message = "Unknown command"
                                };
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        response = new Response()
                        {
                            Status = "ERROR",
                            Message = e.Message
                        };
                    }
                }
            }

            try
            {
                Log($"Response to client {response}");
                ServerManager.SendResponseToClient(clientContext, response);
            }
            catch (Exception e)
            {
                Log("Connection with client error. Connection will be close");
                Log($"Error {e.Message}");
            }

        }

        static void Main(string[] args)
        {
            ServerManager serverManager = new ServerManager();

            while (true)
            {
                HttpListenerContext clientContext = serverManager.GetClientContext();
                Task.Factory.StartNew(() => { ProccessClient(clientContext); });
            }
        }
    }
}
