﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CommunicationClassLibrary;
using Newtonsoft.Json;

namespace ClientClassLibrary
{
    public class ClientManager
    {
        private HttpClient httpClient;

        public ClientManager()
        {
            httpClient = new HttpClient();
        }

        public async Task<Response> MakeRequestToServer(Request request)
        {
            string requestData = JsonConvert.SerializeObject(request);

            var content = new StringContent(requestData, Encoding.UTF8, "application/json");

            HttpResponseMessage responseContext = await httpClient.PostAsync("http://localhost:12345/connection/", content);

            string responseData = await responseContext.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Response>(responseData);
        }
    }
}
