﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationClassLibrary
{
    public class Request
    {
        public string Command { get; set; }
        public string Parameters { get; set; }
        public string Key { get; set; }

        public override string ToString()
        {
            return $"Command: {Command}\nParameters: {Parameters}";
        }
    }
}
