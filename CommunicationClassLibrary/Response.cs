﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationClassLibrary
{
    public class Response
    {
        public string Status { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return $"Status: {Status}\nMessage: {Message}";
        }
    }
}
