﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManagerClassLibrary.Tables;

namespace DbManagerClassLibrary
{
    public class DbManager
    {
        public TableCards TableCards { get; private set; }
        public TableUserCards TableUserCards { get; private set; }
        public TableUsers TableUsers { get; private set; }

        public DbManager()
        {
            TableCards = new TableCards();
            TableUserCards = new TableUserCards();
            TableUsers = new TableUsers();
        }
    }
}
